<?php

namespace Yr\Default\Controller;

class Autoload
{
    const arSettings = [
        'Ml\\' => '/local/classes/',
        'Ml\\Events\\' => '/local/php_interface/events/'
    ];
    public static function register()
    {
        spl_autoload_register(__CLASS__."::callback");
    }

    public static function callback($class)
    {
         $arSettings = self::arSettings;
        if(defined("SETTINGS_AUTOLOAD")){
            $arSettings = SETTINGS_AUTOLOAD;
        }

        foreach ($arSettings as $prefix => $base_dir) {
            $len = strlen($prefix);
            if (strncmp($prefix, $class, $len) !== 0) {
                // no, move to the next registered autoloader
                return;
            }
            $relative_class = substr($class, $len);
            $file = $_SERVER['DOCUMENT_ROOT'].$base_dir . str_replace('\\', '/', $relative_class) . '.php';

            if (file_exists($file)) {
                require $file;
            }
        }
    }
}
