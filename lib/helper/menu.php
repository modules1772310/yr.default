<?php

namespace Yr\Default\Helper;

use Bitrix\Main\Localization\Loc;
class Menu
{
    public static function onBuildGlobalMenu(&$arGlobalMenu, &$arModuleMenu)
    {
        if (!isset($arGlobalMenu['global_menu_yr'])) {
            $arGlobalMenu['global_menu_yr'] = [
                'menu_id' => 'yr',
                'text' => 'YR Modules',
                'title' => 'YR Modules',
                'sort' => 99999,
                'items_id' => 'global_menu_yr_items',
                "icon" => "",
                "page_icon" => "",
            ];
        }
        $aMenu = array(
            "parent_menu" => 'global_menu_yr',
            "section" => 'yr.default',
            "sort" => 200,
            "text" => "yr.default",
            "title" => "yr.default",
            "icon" => "sys_menu_icon",
            "page_icon" => "sys_menu_icon",
            'more_url' => array(
                "yr.default.php",
            ),
            "items_id" => "menu_yr.default",
            'items' => array(
                array(
                    "icon" => "fav_menu_icon_yellow",
                    "page_icon" => "fav_menu_icon_yellow",
                    "text" => "Settings Default",
                    "title" =>"Settings Default",
                    "url" => "settings.php?lang=ru&mid=yr.default&mid_menu=1",
                    "more_url" => array(),
                ),
            )
        );
        $arGlobalMenu['global_menu_yr']['items']['yr.default'] = $aMenu;
    }
}