<?
/**
 * CControllerClient::GetInstalledOptions($module_id);
 * формат массива, элементы:
 * 1) ID опции (id инпута)(Берется с помощью COption::GetOptionString($module_id, $Option[0], $Option[2]) если есть)
 * 2) Отображаемое имя опции
 * 3) Значение по умолчанию (так же берется если первый элемент равен пустой строке), зависит от типа:
 *      checkbox - Y если выбран
 *      text/password - htmlspecialcharsbx($val)
 *      selectbox - одно из значений, указанных в массиве опций
 *      multiselectbox - значения через запятую, указанные в массиве опций
 * 4) Тип поля (массив)
 *      1) Тип (multiselectbox, textarea, statictext, statichtml, checkbox, text, password, selectbox)
 *      2) Зависит от типа:
 *         text/password - атрибут size
 *         textarea - атрибут rows
 *         selectbox/multiselectbox - массив опций формата ["Значение"=>"Название"]
 *      3) Зависит от типа:
 *         checkbox - доп атрибут для input (просто вставляется строкой в атрибуты input)
 *         textarea - атрибут cols
 *
 *      noautocomplete) для text/password, если true то атрибут autocomplete="new-password"
 *
 * 5) Disabled = 'Y' || 'N';
 * 6) $sup_text - ??? текст маленького красного примечания над названием опции
 * 7) $isChoiceSites - Нужно ли выбрать сайт??? флаг Y или N
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;


Loc::loadMessages(__FILE__);

global $APPLICATION;

$request = Application::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);


$POST_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($POST_RIGHT < "S") {
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}
Loader::includeModule($module_id);

$arTabs = array(
    array(
        // значение будет вставленно во все элементы вкладки для идентификации (используется для javascript)
        "DIV" => "settings", // название вкладки в табах
        "TAB" => "Настройки", // заголовок и всплывающее сообщение вкладки
        "TITLE" => "Настройки", // массив с опциями секции
        "OPTIONS" => array(
            "Тест",
            array(

                "test",// имя элемента формы, для хранения в бд
                "test_2", // поясняющий текст
                "",  // значение по умолчани, значение checkbox по умолчанию "Да"
                Array("textarea", 5,80)// тип элемента формы "textarea"
            ),
            "Название секции text",
            array(
                "hmarketing_text", // имя элемента формы, для хранения в бд
                "Поясняющий текс элемента text",  // поясняющий текст
                "Жми!",  // значение по умолчани, значение text по умолчанию "50"

                array("text", 10, 50) // тип элемента формы "text", ширина, высота
            ),
            "Название секции selectbox",
            array(
                "hmarketing_selectbox", // имя элемента формы, для хранения в бд
                "Поясняющий текс элемента selectbox", // поясняющий текст
                "460",// значение по умолчани, значение selectbox по умолчанию "left"

                array("selectbox", [// тип элемента формы "select"
                    // доступные значения
                    "460" => "460Х306",
                    "360" => "360Х242",
                    ]
                )
            ),
            "Название секции multiselectbox",
            array(
                "hmarketing_multiselectbox", // имя элемента формы, для хранения в бд
                "Поясняющий текс элемента multiselectbox", // поясняющий текст
                "left, bottom", // значение по умолчани, значение selectbox по умолчанию "left"
                array("multiselectbox", array( // тип элемента формы "multi select"
                    // доступные значения
                    "left" => "Лево",
                    "right" => "Право",
                    "top" => "Верх",
                    "bottom" => "Низ",
                ))
            )
        )
    ),
    array(
         "DIV" => "access",
         "TAB" => "Доступ",
         "TITLE" => "Уровень доступа к модулю",
    )
);
// проверяем текущий POST запрос и сохраняем выбранные пользователем настройки
if ($request->isPost() && check_bitrix_sessid()) {
    foreach ($arTabs as $aTab) {
        foreach ($aTab["OPTIONS"] as $arOption) {
            __AdmSettingsSaveOption($module_id, $arOption);
        }
    }

    $REQUEST_METHOD = "POST";
    ob_start();
    require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/admin/group_rights.php');
    ob_end_clean();

    LocalRedirect($APPLICATION->GetCurPage() . '?lang=' . LANGUAGE_ID . '&mid=' . $module_id);
}


// отрисовываем форму, для этого создаем новый экземпляр класса CAdminTabControl, куда и передаём массив с настройками
$tabControl = new CAdminTabControl("tabControl",$arTabs);
$tabControl->Begin();
?>
    <form action="<?= $APPLICATION->GetCurPage(); ?>?mid=<?= $module_id; ?>&lang=<?= LANG; ?>" method="post">
        <? foreach ($arTabs as $aTab) {
            if ($aTab["OPTIONS"] && $aTab["DIV"] != "access") {
                $tabControl->BeginNextTab();
                __AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
            }
            if ($aTab["DIV"] == "access") {
                $tabControl->BeginNextTab();
                require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
            }
        }
        // подключаем кнопки отправки формы
        $tabControl->Buttons([
            "back_url" => $_REQUEST["back_url"],
            "btnApply" => true,
            "btnSave" => true,
        ]);
        echo bitrix_sessid_post();
        ?>
    </form>
<? $tabControl->End();?>