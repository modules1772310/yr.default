<?php

//подключаем класс и файлы локализации
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
//добавляем пункт меню для нашего модуля
$menu = array(
    array(
        'parent_menu' => 'global_menu_content',//определяем место меню, в данном случае оно находится в главном меню
        'sort' => 400,//сортировка, в каком месте будет находится наш пункт
        'text' => "test",//описание из файла локализации
        'title' => "test",//название из файла локализации
        'url' => 'mymodule_index.php',//ссылка на страницу из меню
        'items_id' => 'menu_references',//описание подпункта, то же, что и ранее, либо другое, можно вставить сколько угодно пунктов меню
        'items' => array(
            array(
                'text' => "test",
                'url' => 'mymodule_index.php?lang=' . LANGUAGE_ID,
                'more_url' => array('mymodule_index.php?lang=' . LANGUAGE_ID),
                'title' => "test",
            ),
        ),
    ),
);

return $menu;