<?php

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;
use \Bitrix\Main\Localization\Loc;


Loc::loadMessages(__FILE__);

class yr_default extends CModule
{
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = 'Y';
    protected $MODULE_DIR_PATH;

    public function __construct()
    {
        $arModuleVersion = [];
        include __DIR__ . '/version.php';

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = str_replace("_", ".", get_class($this));
        $this->MODULE_NAME = "Балванка для создания модуля"; //Наименование модуля
        $this->MODULE_DESCRIPTION = ""; //Описание модуля
        $this->MODULE_GROUP_RIGHTS = 'Y';
        $this->PARTNER_NAME = 'YR';
        $this->PARTNER_URI = '';
        $this->MODULE_DIR_PATH = realpath(dirname(__FILE__) . '/../');
    }

    public function DoInstall()
    {
        try {
            ModuleManager::registerModule($this->MODULE_ID); // Регистрируем модуль в системе
            $this->InstallFiles();
            $this->InstallDB();
            $this->InstallEvents();
            //$this->InstallAgents();

        }
        catch (Exception $exception) {
            $GLOBALS['APPLICATION']->ThrowException(
                $exception->getMessage()
            );
        }
        return true;
    }

    public function doUninstall()
    {
        try {
            $this->UnInstallFiles();
            $this->UnInstallDB();
            $this->UnInstallEvents();
            //$this->UnInstallAgents();
            ModuleManager::unRegisterModule($this->MODULE_ID);
        }
        catch (Exception $exception) {
            $GLOBALS['APPLICATION']->ThrowException(
                $exception->getMessage()
            );
        }

        return true;
    }

    function InstallFiles()
    {
        // скопируем файлы темы модуля в /bitrix/themes
        CopyDirFiles(__DIR__ . '/themes/.default', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/themes/.default/', true, true);
        // скопируем файлы на страницы админки из папки в битрикс, копирует одноименные файлы из одной директории в другую директорию
        CopyDirFiles(__DIR__ . "/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);
        // скопируем компоненты из папки в битрикс, копирует одноименные файлы из одной директории в другую директорию
        CopyDirFiles(__DIR__ . "/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components", true, true);
        // копируем файлы страниц, копирует одноименные файлы из одной директории в другую директорию
        CopyDirFiles(__DIR__ . '/files', $_SERVER["DOCUMENT_ROOT"] . '/', true, true );

        return true;  // для успешного завершения, метод должен вернуть true
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx('/bitrix/themes/.default/icons/' . $this->MODULE_ID);
        DeleteDirFilesEx('/bitrix/themes/.default/' . $this->MODULE_ID . '.css');
        // удалим файлы из папки в битрикс на страницы админки, удаляет одноименные файлы из одной директории, которые были найдены в другой директории, функция не работает рекурсивно
        DeleteDirFiles(__DIR__ . "/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
        // удалим компонент из папки в битрикс
        if (is_dir($_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/" . $this->MODULE_ID)) {
            DeleteDirFilesEx("/bitrix/components/" . $this->MODULE_ID);
        }
        // удалим файлы страниц, удаляет одноименные файлы из одной директории, которые были найдены в другой директории, функция не работает рекурсивно
        DeleteDirFiles(__DIR__ . "/files", $_SERVER["DOCUMENT_ROOT"] . "/");

        return true;
    }

    function InstallEvents()
    {
        $eventManager = EventManager::getInstance();

        $events = $this->getEventsList();
        foreach ($events as $arEvent) {
            $eventManager->registerEventHandler(
                $arEvent['module'],
                $arEvent['event'],
                $this->MODULE_ID,
                $arEvent['class'],
                $arEvent['method']
            );
        }
        return true;
    }

    function UnInstallEvents()
    {
        $eventManager = EventManager::getInstance();

        $events = $this->getEventsList();
        foreach ($events as $arEvent) {
            $eventManager->unRegisterEventHandler(
                $arEvent['module'],
                $arEvent['event'],
                $this->MODULE_ID,
                $arEvent['class'],
                $arEvent['method']
            );
        }

        return true;
    }

    function InstallDB()
    {
        try {
            Loader::includeModule($this->MODULE_ID);
            foreach ($this->getTableList() as $table) {
                $table::getEntity()->createDbTable();
            }
        }
        catch (Exception $exception) {
            $GLOBALS['APPLICATION']->ThrowException(
                $exception->getMessage()
            );
        }

        return true;
    }

    function UnInstallDB()
    {
        try {
            Loader::includeModule($this->MODULE_ID);

            foreach ($this->getTableList() as $table) {
                \Bitrix\Main\Application::getConnection()->dropTable(
                    $table::getEntity()->getDBTableName()
                );
            }
        }
        catch (Exception $exception) {
            $GLOBALS['APPLICATION']->ThrowException(
                $exception->getMessage()
            );
        }

        return true;
    }

    // установка агентов
    function installAgents()
    {
        return false;
    }
    // удаление агентов
    function UnInstallAgents()
    {
        return false;
    }

    /**
     * <code>
     * return array[
     *      '\Test\Module\Model\TestTable'
     *      '\Test\Module\Model\Test2Table'
     * ]
     * </code>
     * @return array[]
     */
    private function getTableList()
    {
        //Новый подход, автоматическое определние ORM классов внутри папки /lib/model/
        $classesBefore = get_declared_classes();
        foreach (glob($this->MODULE_DIR_PATH . "/lib/model/*.php") as $file) {
            require_once($file);
        }
        $classesAfter = get_declared_classes();
        $modelClasses = array_diff($classesAfter, $classesBefore);

        if($modelClasses){
            $modelClasses = array_map(fn($modelClass)=> "\\".$modelClass,$modelClasses);
            return $modelClasses;
        }

        //Старый подход, вручную прописываем какие таблицы создаем
        return [
            '\Test\Module\Model\TestTable',
            '\Test\Module\Model\Test2Table',
        ];
    }

    /**
     * @return array
     *
     *<code>
     * //Пример что возвращает метод
     *
     * $return = [
     *   [
     *    "module" => "main",
     *    "event" => "OnBeforeEventAdd",
     *    "class" => "Ml\Events\Email",
     *    "method" => "onBeforeEventAdd"
     *   ]
     * ];
     *<code>
     *
     */
    private function getEventsList()
    {
        return [
            [
                'module' => 'main',
                'event' => 'OnBuildGlobalMenu',
                'class' => 'Yr\Default\Helper\Menu',
                'method' => 'onBuildGlobalMenu',
            ],
        ];
    }
}

